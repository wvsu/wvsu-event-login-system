## About WVSU Event Login System

This system is a login and logout management system for students who have events in schools.
QR codes should be distributed for every student and will serve as thier login and logout ticket for the said event.
This requres a QR reader app for implementation.

Note: Make sure to point your virtual host folder to public/ folder in order to run the system.

By: [Peter Porras](https://peterporras.com).
