<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentLogs extends Model
{

    protected $table = 'student_logs';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'log_type', // 'login', 'logout', 'breakin', 'breakout'
        'student_id', 
        'event_id',
    ];

    /**
     * Get the phone record associated with the student.
     */
    public function student()
    {
        return $this->hasOne('App\Students', 'id', 'student_id');
    }

    /**
     * Get the phone record associated with the student.
     */
    public function event()
    {
        return $this->hasOne('App\Events', 'id', 'event_id');
    }
}
