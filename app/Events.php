<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Events extends Model
{
    protected $table = 'events';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 
        'description', 
        'date',
    ];

    /**
     * Get the phone record associated with the user.
     */
    public function logs()
    {
        return $this->hasMany('App\StudentLogs', 'event_id');
    }
}
