<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Students extends Model
{
    protected $table = 'students';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 
        'last_name', 
        'year_level',
        'year_section',
        'school_id',
    ];

    /**
     * Get the phone record associated with the user.
     */
    public function logs()
    {
        return $this->hasMany('App\StudentLogs', 'student_id');
    }
}
