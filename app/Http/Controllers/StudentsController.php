<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Students;


class StudentsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {

        $year = ( isset( $_GET['year'] ) ) ? strip_tags($_GET['year']) : null;
        $year = ( $year ) ? $year : null;
        $section = ( isset( $_GET['section'] ) ) ? strip_tags($_GET['section']) : '';
        $section = ( empty( $section ) ) ? null : $section;
        
        if ( $year && is_null( $section ) ) {
        
            $students = Students::where('year_level', '=', $year)
                ->paginate( 10 );
        
        } elseif ( $section && is_null( $year ) ) {
        
            $students = Students::where('year_section', '=', $section)
                ->paginate( 10 );
        
        } elseif ( $year && $section ) {
            
            $students = Students::where([
                ['year_level', '=', $year],
                ['year_section', '=', $section ],
            ])->paginate( 10 );
        
        } else {

            $students = Students::paginate( 10 );
        }

        return view( 'students', [
            'students' => $students,
            'year' => $year,
            'section' => $section,
        ]);
    }

    public function create()
    {

        
    }

    public function delete()
    {

    }

}
