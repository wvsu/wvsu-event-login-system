<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Students;
use App\StudentLogs;
use App\Events;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->args = [
            'students' => null,
            'error' => false,
            'message' => '',
        ];

        $this->event = Events::where( 'date', '=', date('m/d/Y') )->first();

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $total_students = Students::count();

        return view('home', [
            'logged_in' => $this->counter( 'logged_in' ),
            'on_break' => $this->counter( 'on_break' ),
            'logged_out' => $this->counter( 'logged_out' ),
            'absent' => $this->counter( 'absent' ),
            'total_students' => $total_students,
            'event' => $this->event,
        ]);
    }


    public function printPreview() 
    {
        $year = ( isset( $_GET['year'] ) ) ? strip_tags($_GET['year']) : null;
        $year = ( $year ) ? $year : null;
        $section = ( isset( $_GET['section'] ) ) ? strip_tags($_GET['section']) : '';
        $section = ( empty( $section ) ) ? null : $section;
        
        if ( $year && is_null( $section ) ) {
        
            $students = Students::where('year_level', '=', $year)
                ->paginate( 100 );
        
        } elseif ( $section && is_null( $year ) ) {
        
            $students = Students::where('year_section', '=', $section)
                ->paginate( 100 );
        
        } elseif ( $year && $section ) {
            
            $students = Students::where([
                ['year_level', '=', $year],
                ['year_section', '=', $section],
            ])->paginate( 100 );
        
        } else {

            $students = Students::paginate( 20 );
        }

        return view( 'layouts.print', [
            'students' => $students,
            'year' => $year,
            'section' => $section,
        ]);
    }

    public function logged_in( $event_id, Request $request )
    {

        $event = Events::find( $event_id );

        $year = $request->input('year');
        $section = $request->input('section');
        $per_page = preg_replace('/\D/', '', $request->input('per_page'));
        $per_page = ( $per_page ) ? $per_page : 10;
        $this->args['per_page'] = $per_page;
        $this->args['year'] = $year;
        $this->args['section'] = $section;

        if ( ! $event ) {
            
            $this->args['error'] = true;
            $this->args['message'] = 'No active students to show at the moment.';

        } else {

            $students = StudentLogs::where([
                ['log_type', '=', 'login'],
                ['event_id', '=', $event->id],
            ]);

            if ( $section && $year ) {

                $students->whereHas('student', function($query) use ($section, $year)  {
                    $query->where('year_section', '=', $section)
                        ->where('year_level', '=', $year);
                });

            } elseif ( $year ) {

                $students->whereHas('student', function($query) use ($year)  {
                    $query->where('year_level', '=', $year);
                });

            } elseif ( $section ) {

                $students->whereHas('student', function($query) use ($section)  {
                    $query->where('year_section', '=', $section);
                });

            }

            $this->args['students'] = $students->paginate( $per_page );
            $this->args['event'] = $event;
        }

        return view( 'logged.in', $this->args );
    }

    public function logged_out( $event_id, Request $request )
    {
        $event = Events::find( $event_id );

        $year = $request->input('year');
        $section = $request->input('section');
        $per_page = preg_replace('/\D/', '', $request->input('per_page'));
        $per_page = ( $per_page ) ? $per_page : 10;
        $this->args['per_page'] = $per_page;
        $this->args['year'] = $year;
        $this->args['section'] = $section;

        if ( ! $event ) {
            
            $this->args['error'] = true;
            $this->args['message'] = 'No active students to show at the moment.';

        } else {


            $students = StudentLogs::where([
                ['log_type', '=', 'logout'],
                ['event_id', '=', $event->id],
            ]);

            if ( $section && $year ) {

                $students->whereHas('student', function($query) use ($section, $year)  {
                    $query->where('year_section', '=', $section)
                        ->where('year_level', '=', $year);
                });

            } elseif ( $year ) {

                $students->whereHas('student', function($query) use ($year)  {
                    $query->where('year_level', '=', $year);
                });

            } elseif ( $section ) {

                $students->whereHas('student', function($query) use ($section)  {
                    $query->where('year_section', '=', $section);
                });

            }

            $this->args['students'] = $students->paginate( $per_page );
            $this->args['event'] = $event;
        }

        return view( 'logged.out', $this->args );
    }

    public function on_break( $event_id, Request $request )
    {
        $event = Events::find( $event_id );

        $year = $request->input('year');
        $section = $request->input('section');
        $per_page = preg_replace('/\D/', '', $request->input('per_page'));
        $per_page = ( $per_page ) ? $per_page : 10;
        $this->args['per_page'] = $per_page;
        $this->args['year'] = $year;
        $this->args['section'] = $section;

        if ( ! $event ) {
            
            $this->args['error'] = true;
            $this->args['message'] = 'No active students to show at the moment.';

        } else {

            $went_out = Students::whereHas('logs', function($query) use ($event_id)  {
                $query->where([
                    ['event_id', '=', $event_id],
                    ['log_type', '=', 'breakin'],
                ]);
            })->pluck('id')->all();

            $went_back = Students::whereHas('logs', function($query) use ($event_id)  {
                $query->where([
                    ['event_id', '=', $event_id],
                    ['log_type', '=', 'breakout'],
                ]);
            })->pluck('id')->all();

            // fetch all student ID's who never went back.
            $out = array_diff( $went_out, $went_back );
            $students = StudentLogs::whereIn('student_id', $out)->where([
                ['event_id', '=', $event_id],
                ['log_type', '=', 'breakin'],
            ]);

            if ( $section && $year ) {

                $students->whereHas('student', function($query) use ($section, $year)  {
                    $query->where('year_section', '=', $section)
                        ->where('year_level', '=', $year);
                });

            } elseif ( $year ) {

                $students->whereHas('student', function($query) use ($year)  {
                    $query->where('year_level', '=', $year);
                });

            } elseif ( $section ) {

                $students->whereHas('student', function($query) use ($section)  {
                    $query->where('year_section', '=', $section);
                });

            }

            $this->args['students'] = $students->paginate( $per_page );
            $this->args['event'] = $event;
        }

        return view( 'logged.break', $this->args );
    }

    public function absent( $event_id, Request $request )
    {
        $event = Events::find( $event_id );

        $year = $request->input('year');
        $section = $request->input('section');
        $per_page = preg_replace('/\D/', '', $request->input('per_page'));
        $per_page = ( $per_page ) ? $per_page : 10;
        $this->args['per_page'] = $per_page;
        $this->args['year'] = $year;
        $this->args['section'] = $section;

        if ( ! $event ) {
            
            $this->args['error'] = true;
            $this->args['message'] = 'No active students to show at the moment.';

        } else {

            // fetch all students who attended the event.
            $studentlogs = StudentLogs::where( 'event_id','=', $event->id )->pluck('student_id')->all();
            // fetch all students who were not able to attend the event.
            $students = Students::whereNotIn('id', $studentlogs);

            if ( $section && $year ) {

                $students->where('year_section', '=', $section)->where('year_level', '=', $year);

            } elseif ( $year ) {

                $students->where('year_level', '=', $year);

            } elseif ( $section ) {

                $students->where('year_section', '=', $section);

            }

            $this->args['students'] = $students->paginate( $per_page );
            $this->args['event'] = $event;
        }

        return view( 'logged.absent', $this->args );
    }

    public function counter( $type )
    {

        if ( ! $this->event ) {
            return 0;
        }

        switch ( $type ) {
            case 'absent':
                $studentlogs = StudentLogs::where('event_id','=', $this->event->id)->pluck('student_id')->all();
                $students = Students::whereNotIn('id', $studentlogs)->count();
                return $students;
                break;
            case 'logged_in':
                return StudentLogs::where([
                    ['log_type', '=', 'login'],
                    ['event_id', '=', $this->event->id],
                ])->count();
                break;
            case 'on_break':
                $went_out = StudentLogs::where([
                    ['log_type', '=', 'breakin'],
                    ['event_id', '=', $this->event->id],
                ])->get();
                $students = 0;
                // check if the student returned.
                foreach ( $went_out as $in ) {
                    $student = StudentLogs::where([
                        ['student_id', '=', $in->student->id],
                        ['log_type', '=', 'breakout'],
                        ['event_id', '=', $this->event->id],
                    ])->first();
                    // get student if didnt return.
                    if ( !$student  ) { $students++; }
                }
                return $students;
                break;
            case 'logged_out':
                return StudentLogs::where([
                    ['log_type', '=', 'logout'],
                    ['event_id', '=', $this->event->id],
                ])->count();
                break;
            default:
                return 0;
                break;
        }
    }

    public function printList()
    {

        $type_list = array('absent','logged_in','on_break','logged_out');

        $pass = ( isset( $_GET['type'] ) && in_array($_GET['type'], $type_list)) ? true : false;
        $type = ( $pass ) ? strip_tags($_GET['type']) : null;
        
        if ( !$this->event || !$pass ) {
            return view( 'layouts.list', [
                'students' => null,
            ]);
        }

        $data = '';
        switch ( $type ) {
            case 'absent':
                $studentlogs = StudentLogs::where('event_id','=', $this->event->id)->pluck('student_id')->all();
                $students = Students::whereNotIn('id', $studentlogs)->get();
                $data = $students;
                break;
            case 'logged_in':
                $data = StudentLogs::where([
                    ['log_type', '=', 'login'],
                    ['event_id', '=', $this->event->id],
                ])->get();
                break;
            case 'on_break':
                $went_out = StudentLogs::where([
                    ['log_type', '=', 'breakin'],
                    ['event_id', '=', $this->event->id],
                ])->get();
                $students = array();
                // check if the student returned.
                foreach ( $went_out as $in ) {
                    $student = StudentLogs::where([
                        ['student_id', '=', $in->student->id],
                        ['log_type', '=', 'breakout'],
                        ['event_id', '=', $this->event->id],
                    ])->first();
                    // get student if didnt return.
                    if ( !$student  ) { 
                        $in->student->created_at = $in->created_at;
                        $students[] = $in->student;
                    }
                }
                $data = $students;
                break;
            case 'logged_out':
                $data = StudentLogs::where([
                    ['log_type', '=', 'logout'],
                    ['event_id', '=', $this->event->id],
                ])->get();
                break;
        }

        return view( 'layouts.list', [
            'students' => $data,
            'type' => $type,
        ]);
    }
}
