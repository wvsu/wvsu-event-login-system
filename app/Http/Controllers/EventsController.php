<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\Events;
use App\StudentLogs;
use App\Students;

class EventsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Events::orderBy('date', 'asc')->paginate(10);

        return view( 'events', [ 'events' => $events ] );
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request )
    {

        $validator = Validator::make( $request->all(), [
            'title' => 'required|max:255', // All titles should not be unique.
            'description' => '', // Description is optional.
            'date' => 'required|unique:events', // check if there are events on the same date.
        ]);

        if ( $validator->fails() ) {
            return redirect('events')
                ->withErrors( $validator )
                ->withInput();
        }
        
        $gallery = new Events();
        $gallery->fill( $request->all() );
        $gallery->save();

        return redirect('events')
                ->with('status','Event Successfully created!');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function update( Request $request, $id )
    {

        $event = Events::find( $id );
        if ( ! $event ) {
            return redirect('events')
                ->with('error','Event not found!');
        }

        $validator = Validator::make( $request->all(), [
            'title' => 'required|max:255', // All titles should not be unique.
            'description' => '', // Description is optional.
            'date' => 'required|unique:events', // check if there are events on the same date.
        ]);

        if ( $validator->fails() ) {
            return redirect( 'events/' . $id . '/edit' )
                ->withErrors( $validator )
                ->withInput();
        }
        
        $event->fill( $request->all() );
        $event->save();

        return redirect( 'events/' . $id . '/edit' )
                ->with('status','Event Successfully updated!');
    }

    public function edit( $id )
    {

        $event = Events::find( $id );
        if ( ! $event ) {
            return redirect('events')
                ->with('error','Event not found!');
        }

        return view( 'events.edit', [ 'event' => $event ] );
    }

    public function show( $id )
    {

        $event = Events::find( $id );

        if ( ! $event ) {
            return redirect('events')
                ->with('error','Event not found!');
        }

        $students = StudentLogs::where([
            ['log_type', '=', 'login'],
            ['event_id', '=', $event->id],
        ])->paginate( 10 );

        $total_students = Students::count();

        return view( 'events.show', [ 
            'logged_in' => $this->counter( $event->id, 'logged_in' ),
            'on_break' => $this->counter( $event->id, 'on_break' ),
            'logged_out' => $this->counter( $event->id, 'logged_out' ),
            'absent' => $this->counter( $event->id, 'absent' ),
            'total_students' => $total_students,
            'event' => $event,
            'students' => $students,
        ] );
    }

    public function counter( $event_id, $type )
    {

        $event = Events::find( $event_id );

        if ( ! $event ) {
            return 0;
        }

        switch ( $type ) {
            case 'absent':
                
                $studentlogs = StudentLogs::where( 'event_id','=', $event->id )->pluck('student_id')->all();
                $students = Students::whereNotIn( 'id', $studentlogs )->count();
                
                return $students;

                break;
            case 'logged_in':

                return StudentLogs::where([
                    ['log_type', '=', 'login'],
                    ['event_id', '=', $event->id],
                ])->count();

                break;
            case 'on_break':

                $went_out = StudentLogs::where([
                    ['log_type', '=', 'breakin'],
                    ['event_id', '=', $event->id],
                ])->get();

                $students = 0;

                // check if the student returned.
                foreach ( $went_out as $in ) {
                    $student = StudentLogs::where([
                        ['student_id', '=', $in->student->id],
                        ['log_type', '=', 'breakout'],
                        ['event_id', '=', $event->id],
                    ])->first();
                    // get student if didnt return.
                    if ( !$student  ) { $students++; }
                }

                return $students;

                break;
            case 'logged_out':
                
                return StudentLogs::where([
                    ['log_type', '=', 'logout'],
                    ['event_id', '=', $event->id],
                ])->count();

                break;
            default:
                return 0;
                break;
        }
    }

    
}
