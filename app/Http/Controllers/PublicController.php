<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\StudentLogs;
use App\Students;
use App\Events;

class PublicController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    	// $this->middleware('guest');

        $this->return = array(
            'error' => false,
            'message' => ''
        );

        $this->event = Events::where( 'date', '=', date('m/d/Y') )->first();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$key = env('API_KEY');

    	$has_key = ( isset( $_GET['key'] ) && $key == $_GET['key'] ) ? true : false;
        $fullname = ( isset( $_GET['name'] ) ) ? strip_tags( $_GET['name'] ) : '';
        $type = ( isset( $_GET['type'] ) ) ? strip_tags( $_GET['type'] ) : '';
        $type_list = array('login','logout','breakin','breakout');

        if ( ! $has_key || empty( $fullname ) || empty( $type ) || !$this->event || !in_array( $type, $type_list ) ) {
            $this->return['error'] = true;
            $this->return['message'] = 'Please try again.';
        	return $this->return;
        }

        $name = explode(' ', $fullname);
        $last = count( $name )-1;
        $first_name = '';
        $last_name = '';
        $conter = 0;
        foreach ( $name as $n ) {
            if ( $conter == $last ) {
                $last_name = $n;
            } else {
                $first_name .= $n.' ';
            }
            $conter++;
        }

        $hash = md5($fullname);

        $student = Students::where([
            ['hash', '=', $hash]
        ])->first();

        if ( $student ) {
            
            $message = 'Take A break first!';

            $student_log = StudentLogs::where([
                ['student_id', '=', $student->id],
                ['event_id', '=', $this->event->id],
                ['log_type', '=', $type],
            ])->first();

            $is_loggedin = StudentLogs::where([
                ['student_id', '=', $student->id],
                ['event_id', '=', $this->event->id],
                ['log_type', '=', 'login'],
            ])->first();

            $is_onbreak = StudentLogs::where([
                ['student_id', '=', $student->id],
                ['event_id', '=', $this->event->id],
                ['log_type', '=', 'breakin'],
            ])->first();

            switch ( $type ) {
                case 'login': 

                    if ( ! $student_log ) {

                        $log = new StudentLogs();
                        $log->fill(array(
                            'student_id' => $student->id,
                            'event_id' => $this->event->id,
                            'log_type' => $type,
                        ));
                        $log->save();

                        $message = 'Welcome ' . $name[0] . '!'; 

                    } else {

                        $message = $name[0] . ' is already signed in!'; 
                    }
                    break;

                case 'logout': 

                    if ( ! $is_loggedin ) {

                        $message = 'Sorry but you need to login first ' . $name[0] . ' before logging out.'; 

                    } else {

                        $log = new StudentLogs();
                        $log->fill(array(
                            'student_id' => $student->id,
                            'event_id' => $this->event->id,
                            'log_type' => $type,
                        ));
                        $log->save();
                        
                        $message = 'Take care ' . $name[0] . '!'; 
                    }
                    
                    break;
                case 'breakout': 

                    if ( ! $is_loggedin ) {

                        $message = 'Sorry but you need to login first ' . $name[0] . '.'; 

                    } elseif ( $is_onbreak ) {

                        $log = new StudentLogs();
                        $log->fill(array(
                            'student_id' => $student->id,
                            'event_id' => $this->event->id,
                            'log_type' => $type,
                        ));
                        $log->save();
                        
                        $message = 'Welcome back ' . $name[0] . '!';
                    }
                    
                    break;
                case 'breakin': 

                    if ( ! $is_loggedin ) {

                        $message = 'Sorry but you need to login first ' . $name[0] . '.'; 

                    } else {

                        $log = new StudentLogs();
                        $log->fill(array(
                            'student_id' => $student->id,
                            'event_id' => $this->event->id,
                            'log_type' => $type,
                        ));
                        $log->save();
                        
                        $message = 'Be back soon ' . $name[0] . '!';
                    }

                    break;                
            }

            $this->return['message'] = $message;

            return $this->return;
        
        } else {

            $this->return['error'] = true;
            $this->return['message'] = 'Student not found, please try again.';
            return $this->return;
        }
    }
}
