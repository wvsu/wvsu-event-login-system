<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/fetch', 'PublicController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/print', 'HomeController@printPreview' )->name('print');
Route::get('/list', 'HomeController@printList' )->name('list');
Route::get('/in/{event_id}', 'HomeController@logged_in' )->name('in');
Route::get('/out/{event_id}', 'HomeController@logged_out' )->name('ount');
Route::get('/break/{event_id}', 'HomeController@on_break' )->name('break');
Route::get('/absent/{event_id}', 'HomeController@absent' )->name('absent');

Route::resource('/students', 'StudentsController');
Route::resource('/events', 'EventsController');
