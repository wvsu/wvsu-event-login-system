jQuery(document).ready(function(){
	var $ = jQuery;
	$( ".datepicker" ).datepicker();

	var year_section = {
		0:['A','B'],
		1:['A','B'],
		2:['A','B','C'],
		3:['A','B']
	};

	jQuery('#year-level').change(function( event ) {
		/* Act on the event */
		var val = jQuery(this).val();
		jQuery('#class-section').html('');

		var html = '<option value="">---</option>';

		switch(val) {
			case '3':
				for (var i = year_section[2].length - 1; i >= 0; i--) {
					html += '<option value="' + year_section[2][i] + '"> Section ' + year_section[2][i] + '</option>';
				}
			break;
			default:
				for (var i = year_section[1].length - 1; i >= 0; i--) {
					html += '<option value="' + year_section[1][i] + '"> Section ' + year_section[1][i] + '</option>';
				}
			break;
		}

		jQuery('#class-section').html(html);
		
	});

	jQuery('.click-modal').click(function(event) {
		var button = jQuery(this);
		var url = button.data('src');
		var modal = jQuery('#myModal');
		modal.find('.modal-body').html('<img src="' + url + '" class="img-responsive" alt="" />');
	});

});
