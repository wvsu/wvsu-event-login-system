@extends('layouts.app')

@section('content')
<div class="container">
    
    @include('inc.navmenu')

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Student List</div>
                <div class="panel-body">
                    
                    <div class="row">
                        <div class="col-md-6">

                            @include('inc.filter')
                            
                        </div>
                        <div class="col-md-6">
                            <a href="#" style="margin-right:15px;" class="pull-right btn-info btn">Add Student</a>
                            <a style="margin-right:15px;" class="btn btn-default pull-right" href="{{ url('/') . '/print?&year=' . $year .'&section=' . $section }}">Print QR Codes</a>
                        </div>
                    </div>

                    <hr>
                    
                    @if( $students->count() )
                    
                    <p>Total number of students: <strong>{{ $students->total() }}</strong></p>

                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Year & Section</th>
                                <th>QR Code</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ( $students as $student )
                            <tr>
                                <td>{{ $student->first_name . ' ' . $student->last_name }}</td>
                                <td>{{ $student->year_level . ' - ' . $student->year_section  }}</td>
                                <td>
                                    <a class="btn btn-primary btn-xs click-modal" data-toggle="modal" data-src="{{ url('/') . $student->qr_code }}" href='#myModal'>View</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    {{ $students->appends(request()->input())->links() }}

                    @else
                    
                    <div class="alert alert-warning">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <strong>Error:</strong> No students found.
                    </div>

                    @endif

                    <div class="modal fade" id="myModal">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">QR Code</h4>
                                </div>
                                <div class="modal-body" align="center"></div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
