
<form action="" method="GET" class="form-inline" role="form">
    <div class="form-group">
        <span>Per Page:</span>
        <input style="width: 82px;" type="number" name="per_page" id="per-page" class="form-control" 
            value="{{ (isset( $per_page ) && $per_page ) ? $per_page : 10 }}" min="10" max="" step="" required="required" title="">
    </div>
    <div class="form-group">
        <label class="sr-only" for="">Year</label>
        <select name="year" class="form-control" id="year-level">
            <option value="0">Year</option>
            <option value="1"{{ ( isset( $year ) && $year == '1' ) ? ' selected':'' }}>1<sup>st</sup> Year</option>
            <option value="2"{{ ( isset( $year ) && $year == '2' ) ? ' selected':'' }}>2<sup>nd</sup> Year</option>
            <option value="3"{{ ( isset( $year ) && $year == '3' ) ? ' selected':'' }}>3<sup>rd</sup> Year</option>
            <option value="4"{{ ( isset( $year ) && $year == '4' ) ? ' selected':'' }}>4<sup>th</sup> Year</option>
        </select>
    </div>
    <div class="form-group">
        <label class="sr-only" for="">Section</label>
        <select name="section" class="form-control" id="class-section">
            <option value="">Section</option>
            <option value="A"{{ ( isset( $section ) && $section == 'A' ) ? ' selected':'' }}>Section A</option>
            <option value="B"{{ ( isset( $section ) && $section == 'B' ) ? ' selected':'' }}>Section B</option>
            <option value="C"{{ ( isset( $section ) && $section == 'C' ) ? ' selected':'' }}>Section C</option>
        </select>
    </div>
    <button type="submit" class="btn btn-primary">Filter</button>
</form>