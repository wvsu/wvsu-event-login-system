<div class="col-md-12">
	@if( isset( $event ) && $event )
       <div class="jumbotron">
            <div class="container">
                <h1>{{ $event->title }}</h1>
                <p>{{ $event->description }}</p>
                <p><small>{{ date('F j, Y (l)', strtotime($event->date) ) }}</small></p>
            </div>
        </div>
    @endif
</div>