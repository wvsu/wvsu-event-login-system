<nav class="navbar navbar-default">
    <div class="container-fluid">
        <ul class="nav navbar-nav">
            <li{{ Request::is('home') ? ' class=active' : '' }}>
                <a href="{{ url('home') }}">Dashboard</a>
            </li>
            <li{{ Request::is('students') ? ' class=active' : '' }}>
                <a href="{{ url('students') }}">Students</a>
            </li>
            <li{{ Request::is('events') ? ' class=active' : '' }}>
                <a href="{{ url('events') }}">Events</a>
            </li>
        </ul>
    </div>
</nav>