<div class="col-md-12">

    @foreach ( $errors->all() as $message )
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>Error:</strong> {{ $message }}
        </div>
    @endforeach

    @if ( isset( $status ) )
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>Success:</strong> {{ $status }}
        </div>
    @endif

    @if ( isset( $error ) )
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>Error:</strong> {{ $error }}
        </div>
    @endif

</div>