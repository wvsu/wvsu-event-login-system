@extends('layouts.app')

@section('content')
<div class="container">

    @include('inc.navmenu')

    <div class="row">
        
        @include('inc.jumborton')

        <div class="col-md-12">
            <div role="alert" class="alert alert-danger">
                <h3 class="alert-heading">Absent Students</h3> 
                <p>Students who didn't attend the said event for some unknown reaseon.</p> 
            </div>
        </div>

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Absent Student</div>
                <div class="panel-body">
                    
                    <div class="row">
                        <div class="col-md-6">
                            @include('inc.filter')
                        </div>
                        <div class="col-md-6">
                            <a href="{{ url('list') }}?type=absent" class="btn btn-success pull-right">Print</a>
                        </div>
                    </div>
                    <hr>

					@if( count($students) )

                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Year & Section</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ( $students as $student )
                                <tr>
                                    <td>{{ $student->first_name . ' ' . $student->last_name }}</td>
                                    <td>{{ $student->year_level . ' - ' . $student->year_section  }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        
                        {{ $students->appends(request()->input())->links() }}

                    @endif

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
