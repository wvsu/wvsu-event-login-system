@extends('layouts.app')

@section('content')
<div class="container">
    
    @include('inc.navmenu')

    <div class="row">

        @include('inc.jumborton')

        <div class="col-md-12">
            <div role="alert" class="alert alert-warning">
                <h3 class="alert-heading">On break</h3> 
                <p>Students who went outside the venue and never came back.</p> 
            </div>
        </div>
        
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Student on Break</div>
                <div class="panel-body">
					
                    <div class="row">
                        <div class="col-md-6">
                            @include('inc.filter')
                        </div>
                        <div class="col-md-6">
                            <a href="{{ url('list') }}?type=on_break" class="btn btn-success pull-right">Print</a>
                        </div>
                    </div>
                    <hr>


                    @if( $error ) 
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <strong>Error!</strong> {{ $message }}
                        </div>
                    @endif
                    
                    <p>List of students who went out and never came back during the event.</p>

                    @if( $students->count() )

                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Year & Section</th>
                                    <th>Log time</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ( $students as $student )
                                <tr>
                                    <td>{{ $student->student->first_name . ' ' . $student->student->last_name }}</td>
                                    <td>{{ $student->student->year_level . ' - ' . $student->student->year_section  }}</td>
                                    <td>{{ date('g:i:s A', strtotime( $student->created_at ) ) }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                        {{ $students->appends(request()->input())->links() }}

                    @endif

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
