@extends('layouts.app')

@section('content')
<div class="container">
    
    @include('inc.navmenu')

    <div class="row">
        
        @include('inc.jumborton')

        <div class="col-md-12">
            <div role="alert" class="alert alert-info">
                <h3 class="alert-heading">Logged out</h3> 
                <p>Number of students who succesfully logged out.</p> 
            </div>
        </div>

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Logged Out Student</div>
                <div class="panel-body">
                    
					@if( $error ) 
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <strong>Error!</strong> {{ $message }}
                        </div>
                    @endif
                    
                    <div class="row">
                        <div class="col-md-6">
                            @include('inc.filter')
                        </div>
                        <div class="col-md-6">
                            <a href="{{ url('list') }}?type=logged_out" class="btn btn-success pull-right">Print</a>
                        </div>
                    </div>

                    <hr>
                    
                    @if( $students->count() )

                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Year & Section</th>
                                    <th>Log time</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ( $students as $student )
                                <tr>
                                    <td>{{ $student->student->first_name . ' ' . $student->student->last_name }}</td>
                                    <td>{{ $student->student->year_level . ' - ' . $student->student->year_section  }}</td>
                                    <td>{{ date('g:i:s A', strtotime( $student->created_at ) ) }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        
                        {{ $students->appends(request()->input())->links() }}
                    @else

                    <div class="alert alert-warning">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <strong>Error!</strong> No records found.
                    </div>

                    @endif

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
