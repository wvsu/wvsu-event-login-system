@extends('layouts.app')

@section('content')
<div class="container">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <ul class="nav navbar-nav">
                <li>
                    <a href="{{ url('home') }}">Dashboard</a>
                </li>
                <li>
                    <a href="{{ url('students') }}">Students</a>
                </li>
                <li class="active">
                    <a href="{{ url('events') }}">Events</a>
                </li>
            </ul>
        </div>
    </nav>
    <div class="row">
        
        @include('inc.messages')

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Edit Event</div>
                <div class="panel-body">
            
                    <form action="{{ url('/') . '/events/' . $event->id }}" method="POST" role="form">
                        <input type="hidden" name="_method" value="PUT">
                        {{ csrf_field() }}
                        
                        <div class="form-group<?php echo ( $errors->has('title') ) ? ' has-error':'' ;?>">
                            <label for="">Title</label>
                            <input type="text" name="title" class="form-control" value="{{ $event->title }}" id="" placeholder="Event Name">
                        </div>

                        <div class="form-group<?php echo ( $errors->has('description') ) ? ' has-error':'' ;?>">
                            <label for="">Description</label>
                            <textarea type="text" name="description" class="form-control" placeholder="Short description about the event">{{ $event->description }}</textarea>
                        </div>
                    
                        <div class="form-group<?php echo ( $errors->has('date') ) ? ' has-error':'' ;?>">
                            <label for="">Date</label>
                            <input type="text" name="date" class="datepicker form-control" id="" value="{{ $event->date }}" placeholder="01-18-2018">
                        </div>
                    
                        <button type="submit" class="btn btn-primary">Save</button>

                    </form>
                       

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
