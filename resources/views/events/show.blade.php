@extends('layouts.app')

@section('content')
<div class="container">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <ul class="nav navbar-nav">
                <li>
                    <a href="{{ url('home') }}">Dashboard</a>
                </li>
                <li>
                    <a href="{{ url('students') }}">Students</a>
                </li>
                <li class="active">
                    <a href="{{ url('events') }}">Events</a>
                </li>
            </ul>
        </div>
    </nav>
    <div class="row">

        <div class="col-md-12">
            <div class="jumbotron">
                <div class="container">
                    <h1>{{ $event->title }}</h1>
                    <p>{{ $event->description }}</p>
                    <p><small>{{ date('F j, Y (l)', strtotime($event->date) ) }}</small></p>
                </div>
            </div>
        </div>
        
        <div class="col-md-3">
            <div class="alert alert-success" role="alert">
                <h3 class="alert-heading">Logged In</h3>
                <p>Students who are currently present within the event.</p>
                <hr>
                <h1>{{ $logged_in }} <a href="{{ url('in') . '/' . $event->id }}" class="pull-right btn btn-success">View List</a></h1>
            </div>
        </div>

        <div class="col-md-3">
            <div class="alert alert-warning" role="alert">
                <h3 class="alert-heading">On break</h3>
                <p>Students who went outside the venue.</p>
                <hr>
                <h1>{{ $on_break }} <a href="{{ url('break') . '/' . $event->id }}" class="pull-right btn btn-warning">View List</a></h1>
            </div>
        </div>

        <div class="col-md-3">
            <div class="alert alert-info" role="alert">
                <h3 class="alert-heading">Logged out</h3>
                <p>Number of students who succesfully logged out.</p>
                <hr>
                <h1>{{ $logged_out }} <a href="{{ url('out') . '/' . $event->id }}" class="pull-right btn btn-info">View List</a></h1>
            </div>
        </div>

        <div class="col-md-3">
            <div class="alert alert-danger" role="alert">
                <h3 class="alert-heading">Absent</h3>
                <p>Students who didn't attend the said event for some unknown reaseon.</p>
                <hr>
                <h1>{{ $absent }} <a href="{{ url('absent') . '/' . $event->id }}" class="pull-right btn btn-danger">View List</a></h1>
            </div>
        </div>

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                Total number of students: <b>{{ ( $total_students ) ? $total_students : '' }}</b>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection
