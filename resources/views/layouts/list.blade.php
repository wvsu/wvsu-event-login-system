

@if( $students )
<style>
	table{
		width: 100%;
	}
	th, tr, td {
		border:1px solid #000;
		text-align: left;
		padding: 5px;

	}
</style>
<div align="center">
<table>
	<thead>
		<tr>
			<th>First Name</th>
			<th>Last Name</th>
			<th>Year & Section</th>
		</tr>
	</thead>
	<tbody>
		@foreach( $students as $student )
		<tr>
			<td>{{ ( $type == 'logged_in' || $type == 'logged_out' ) ? $student->student->first_name : $student->first_name }}</td>
			<td>{{ ( $type == 'logged_in' || $type == 'logged_out' ) ? $student->student->last_name : $student->last_name }}</td>
			<td>{{ ( $type == 'logged_in' || $type == 'logged_out' ) ? $student->student->year_level . ' - ' . $student->student->year_section : $student->year_level . ' - ' . $student->year_section }}</td>
		</tr>
		@endforeach
	</tbody>
</table>
</div>
@endif