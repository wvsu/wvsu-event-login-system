<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('jquery-ui/jquery-ui.min.css') }}" rel="stylesheet">
    <link href="{{ asset('jquery-ui/jquery-ui.structure.min.css') }}" rel="stylesheet">
    <link href="{{ asset('jquery-ui/jquery-ui.theme.min.css') }}" rel="stylesheet">
</head>
<body>
	
	<div class="container">
		<div class="row">
			<p>{{ count($students) }}</p>
			@foreach ( $students as $student )
		    <div class="col-md-3 col-sm-3">
		    	<img class="img-responsive" src="{{ url('/') . $student->qr_code }}" alt="">
		    	<p>{{ $student->first_name . ' ' . $student->last_name }}</p>
		    	<br>
		    </div>
		    @endforeach

		</div>
	</div>
	

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ asset('jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('js/scripts.js') }}"></script>
</body>
</html>


