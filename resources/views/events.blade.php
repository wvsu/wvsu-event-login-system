@extends('layouts.app')

@section('content')
<div class="container">
    
    @include('inc.navmenu')

    <div class="row">
        
        @include('inc.messages')

        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">Create Event</div>
                <div class="panel-body">
                    <form action="" method="POST" role="form">
                        {{ csrf_field() }}
                        
                        <div class="form-group<?php echo ( $errors->has('title') ) ? ' has-error':'' ;?>">
                            <label for="">Title</label>
                            <input type="text" name="title" class="form-control" id="" placeholder="Event Name">
                        </div>

                        <div class="form-group<?php echo ( $errors->has('description') ) ? ' has-error':'' ;?>">
                            <label for="">Description</label>
                            <textarea type="text" name="description" class="form-control" id="" placeholder="Short description about the event"></textarea>
                        </div>
                    
                        <div class="form-group<?php echo ( $errors->has('date') ) ? ' has-error':'' ;?>">
                            <label for="">Date</label>
                            <input type="text" name="date" class="datepicker form-control" id="" placeholder="01-18-2018">
                        </div>
                    
                        <button type="submit" class="btn btn-primary">Create Event</button>

                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">Events List</div>
                <div class="panel-body">

                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Date</th>
                                <th>Status</th>
                                <th>Options</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach( $events as $event )
                                <?php 
                                $is_done  = ( strtotime( $event->date ) < strtotime( date('m/d/Y') ) ); 
                                $is_today = ( strtotime( $event->date ) == strtotime( date('m/d/Y') ) ); 
                                $success  = ( $is_done ) ? ' class=danger' : ' class=info';
                                $status   = ( $is_done ) ? 'Finished' : 'Upcoming';
                                $button   = '';
                                if ( $is_today ) {
                                    $success = ' class=success';
                                    $status  = 'Active';
                                }
                                ?>
                                <tr{{ $success }}>
                                    <td>{{ $event->title }}</td>
                                    <td>{{ $event->description }}</td>
                                    <td>{{ $event->date }}</td>
                                    <td>{{ $status }}</td>
                                    <td>
                                        @if ( $is_today ) 
                                            <a href="{{ env('APP_URL') . '/events/' . $event->id }}" class="btn btn-xs btn-info">Details</a>
                                        @elseif ( $is_done )
                                            <a href="{{ env('APP_URL') . '/events/' . $event->id }}" class="btn btn-xs btn-info">Details</a>
                                        @else
                                            <a href="#" class="btn btn-xs btn-danger">Delete</a>
                                        @endif

                                        <a href="{{ env('APP_URL') . '/events/' . $event->id . '/edit' }}" class="btn btn-xs btn-success">Edit</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $events->links() }}
                </div>
            </div>
        </div>

    </div>
</div>
@endsection
