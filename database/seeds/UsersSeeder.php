<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            array(
	            'name' => 'admin',
	            'email' => 'admin@admin.com',
	            'password' => bcrypt('admin')
        	)
        );
    }
}
